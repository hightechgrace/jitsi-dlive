What information do we collect?

None so far. Should that change, it will be published here. Any significant changes will announced here (though none are expected).

What do we do with any information we collect?

Manually use it for troubleshooting and issue resolution.

How long is any information retained?

10 days from the later of: when it was received, when any issue was resolved.

How do we protect your information?

Do we disclose any collected information to outside parties?

Only in the broadest sense, and then without any reference to specific user.
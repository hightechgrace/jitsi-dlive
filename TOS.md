Anyone can use this at any time for any reason, with the following restrictions:

Any changes to privacy must be clearly and widely published.

Any changes to privacy must be opt-in.

Any changes to these terms will be published here.

Your information can not be transferred to third parties without explicit, opt-in user permission.